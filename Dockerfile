FROM ubuntu:focal-20200925
RUN apt-get update && apt-get install python3-software-properties software-properties-common -y -f && add-apt-repository ppa:pypy/ppa -y
RUN apt-get update && apt-get install -y \
build-essential \
cython \
git \
libbz2-dev \
liblzma-dev \
libncurses5-dev \
python3 \
python3-dev \
python3-setuptools \
zlib1g-dev \
wget 
RUN apt-get install -y python3-pip
RUN apt-get install -y curl libcurl4-openssl-dev
RUN pip3 install numpy cython
RUN wget https://github.com/samtools/samtools/releases/download/1.11/samtools-1.11.tar.bz2 -O samtools.tar.bz2 && \
tar -xjvf samtools.tar.bz2 && cd samtools-1.11 && make && make prefix=/usr/local/bin install && ln -s /usr/local/bin/bin/samtools /usr/bin/samtools
RUN pip3 install stpipeline
WORKDIR /home/
RUN git clone https://github.com/alexdobin/STAR.git && \
 	cd /home/STAR/source && make STAR && mv /home/STAR/bin/Linux_x86_64/STAR /usr/local/bin/ && cd /home && rm -rf /home/STAR 
COPY ./ ./
#RUN mkdir contamination/idx && STAR --runThreadN 32 --runMode genomeGenerate --genomeDir contamination/idx/ --genomeFastaFiles contamination/GFPlib.fa --genomeSAindexNbases 4 && rm ./Log.out
RUN chmod -R 755 /home/* && chmod +X Analyze*
RUN rm -rf /var/lib/apt/lists/*

