# README #

This repository is generated to enable easy docker builds of the Spatial Transcriptomics analysis pipeline. 

### What is this repository for? ###

* Quick summary
* Version v0.1

### How do I get set up? ###

* Summary of set up
With docker installed, this can be built using the standard command:

```
#!shell

docker build -t stpipeline .
```

* Configuration
A folder with the raw reads from Illumina sequences has to be mounted inside the Docker container at startup using the following startup command:

```
#!shell

docker run -v /path/to/raw/reads:/home/data -i --name stpipe stpipeline

```

* Dependencies
No external dependencies should be needed with the exception of Docker.

* How to run tests

While the Docker container is running, connect into a Bash shell using 

```
#!shell

docker exec -i -t stpipe /bin/bash
```

and then modify the "ExampleScript" to fith the parameters of the Raw sequencing files and then execute it using 

```
#!shell

./ExampleScript

```

* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact